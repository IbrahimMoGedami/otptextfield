//
//  ViewController.swift
//  OTPField
//
//  Created by Satyenkumar Mourya on 23/06/19.
//  Copyright © 2019 Satyenkumar Mourya. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var otpContainerView: UIView!
    @IBOutlet weak var testButton: UIButton!
    @IBOutlet weak var label: UILabel!
    
    let otpStackView = OTPStackView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        testButton.isHidden = true
        otpStackView.delegate = self
        otpStackView.setupOtp(otpContainerView: otpContainerView, otpStackView: otpStackView)
    }

    @IBAction func clickedForHighlight(_ sender: UIButton) {
        print("Final OTP : ", otpStackView.getOTP())
        otpStackView.setAllFieldColor(isWarningColor: true, color: .yellow)
    }
    
}

extension ViewController: OTPDelegate {
    
    func didChangeValidity(isValid: Bool) {
        print(" text : \(otpStackView.getOTP())")
//        if otpStackView.getOTP().count == 1 {
//            print(" text : \(otpStackView.getOTP())")
//            label.text = ""
//        }
        label.text = otpStackView.getOTP()
    }
    
}

